# greenlion/php-sql-parser

SQL (non validating) parser w/ focus on MySQL. https://packagist.org/packages/greenlion/php-sql-parser

[![PHPPackages Rank](http://phppackages.org/p/greenlion/php-sql-parser/badge/rank.svg)](http://phppackages.org/p/greenlion/php-sql-parser)
[![PHPPackages Referenced By](http://phppackages.org/p/greenlion/php-sql-parser/badge/referenced-by.svg)](http://phppackages.org/p/greenlion/php-sql-parser)

* [sql parser](https://phppackages.org/s/sql%20parser)

## Official documentation
* [greenlion/PHP-SQL-Parser/wiki](https://github.com/greenlion/PHP-SQL-Parser/wiki)